package middleware

import (
	"dendenmushi/handler"
	"log"
	"net/http"

	"github.com/line/line-bot-sdk-go/linebot"
)

type MiddlewareHandler struct {
	Handler   http.Handler
	BotClient *linebot.Client
}

func (middleware *MiddlewareHandler) MiddlewareBot() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handlers := &handler.BotClient{
			Bot: middleware.BotClient,
		}
		handlers.BotProcessMessage(r)
	})
}

func (middleware *MiddlewareHandler) Middleware() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("Executing middleware")
	})
}

func HandleFunc(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("OK"))
}
