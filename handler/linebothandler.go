package handler

import (
	"dendenmushi/constant"
	"dendenmushi/model"
	"dendenmushi/service"
	"dendenmushi/utils"
	"fmt"
	"log"
	"net/http"

	"github.com/line/line-bot-sdk-go/linebot"
)

type BotClient struct {
	Bot        *linebot.Client
	ReplyToken *string
}

func (client *BotClient) BotReplyMessage(message string) {
	_, err := client.Bot.ReplyMessage(*client.ReplyToken, linebot.NewTextMessage(message)).Do()
	if err != nil {
		log.Print(err)
	}
}

func (client *BotClient) BotPushMessage(to, message string) {
	_, err := client.Bot.PushMessage(to, linebot.NewTextMessage(message)).Do()
	if err != nil {
		log.Print(err)
	}
}

func (client *BotClient) BotProcessMessage(r *http.Request) {
	events, err := client.Bot.ParseRequest(r)
	if err != nil {
		fmt.Println("err: ", err)
	}
	for _, event := range events {
		switch event.Type {
		case linebot.EventTypeMessage:
			switch message := event.Message.(type) {
			case *linebot.TextMessage:
				client.ReplyToken = &event.ReplyToken
				client.processExpenseInfo(message, event.Source.UserID)
			case *linebot.StickerMessage:
				fmt.Println(fmt.Sprintf("got sticker : %v", message.StickerID))
			}
		}
	}
}

func (client *BotClient) processExpenseInfo(event *linebot.TextMessage, userID string) {
	message := utils.TextMessage(event.Text)
	if message.StartWith(constant.BotCommandRemember) {
		rawMsg := model.Message{}
		rawMsg = message.ExtractMessage(constant.SeperatorComma, constant.BotCommandRemember)
		exp, err := rawMsg.RawToModel()
		if err != nil {
			client.BotReplyMessage(err.Error())
		}
		exp.UserID = userID
		service.BotReceiveCmdProocess(exp)

		client.BotReplyMessage("Ok")
	}
}
