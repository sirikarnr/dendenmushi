package handler

import (
	"dendenmushi/model"
	"dendenmushi/repository"
	"dendenmushi/service"
	"fmt"
	"math/rand"
	"strings"

	"github.com/spf13/viper"
)

const (
	replaceFieldName   = "{name}"
	replaceFieldAmount = "{amount}"
)

func (client *BotClient) RemindExpense() {
	results := service.BotRemindSchedulerTicker()
	getRemindMessage()
	for _, reminder := range results {
		client.BotPushMessage(reminder.UserID, createBotMessageReminder(reminder))
	}
}

func createBotMessageReminder(reminder model.Reminder) string {
	message := getRemindMessage()
	replacer := strings.NewReplacer(replaceFieldName, strings.Title(reminder.Title), replaceFieldAmount, fmt.Sprintf("%.2f", reminder.Amount))
	return replacer.Replace(message)
}

//get data from https://docs.google.com/spreadsheets/d/12dxbDoUMlcKw1BN9j8LAOQQw9tPszSd0goWrguR8y5s/edit
func getRemindMessage() string {
	spreadsheetID := viper.GetString("google_api.sheets.conversation.id")
	sheetName := viper.GetString("google_api.sheets.conversation.name")
	dataRange := viper.GetString("google_api.sheets.conversation.range.remind_message")
	remindMessages := repository.ReadGoogleSheet(spreadsheetID, sheetName, dataRange)
	return remindMessages[rand.Intn(len(remindMessages))]
}
