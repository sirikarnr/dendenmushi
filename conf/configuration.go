package conf

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

func InitialConfig() {
	viper.SetConfigType("json")
	viper.AddConfigPath("./conf/")
	viper.SetConfigName("config")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("read config file error: %v\n", err))
	}
}
