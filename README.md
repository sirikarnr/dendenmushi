## Dendenmushi

Linebot service to remind monthly payment. 

- input : chat with bot using `bot remember <type> <name> <duedate> <amount>` text message.
- output : push message 1 day before due date (with random message from google sheet).

## Parameter

- <type> : expense type (maybe loan or credit_card)
- <name> : expense title to display in bot remind message
- <duedate> : payment due date (date in `d`)
- <amount> : money that have to pay

## Example
`bot remember loan Home 10 6800`

## Development Information

- Language: Golang
- Data Storage : postgres / google sheet
- Scheduler: cron
- Server : 