package utils

import (
	"dendenmushi/constant"
	"strings"
)

type TextMessage string

func (message TextMessage) StartWith(prefix string) bool {
	messageLower := strings.ToLower(string(message))
	if strings.HasPrefix(messageLower, prefix) {
		return true
	}
	return false
}

func (message TextMessage) ExtractMessage(sp, prefix string) []string {
	msg := strings.Trim(strings.TrimPrefix(strings.ToLower(string(message)), prefix), constant.SeperatorSpace)
	return strings.Fields(msg)
}
