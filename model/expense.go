package model

import (
	"dendenmushi/constant"
	"errors"
	"fmt"
	"strconv"
)

type Message []string

type Expense struct {
	tableName struct{} `sql:"expense"`
	ID        string   `sql:"id,pk"`
	Type      string   `sql:"type,pk"`
	Name      string   `sql:"name,pk"`
	DueDate   string   `sql:"duedate,pk"`
	Amount    float64  `sql:"amount,type:real"`
	UserID    string   `sql:"userid"`
}

func (r Message) RawToModel() (*Expense, error) {
	var amt float64
	fmt.Println("message: ", r)
	if len(r) < 4 {
		return nil, errors.New(constant.InvalidInputFormatError)
	}
	if amount, err := strconv.ParseFloat(r[3], 64); err == nil {
		amt = amount
	} else {
		fmt.Println("parse float error: ", err)
		return nil, errors.New(constant.InvalidInputFormatError)
	}
	exp := &Expense{
		Type:    r[0],
		Name:    r[1],
		DueDate: r[2],
		Amount:  amt,
	}
	return exp, nil
}
