package model

type User struct {
	tableName struct{} `sql:"user"`
	ID        string   `sql:"id,pk"`
	Name      string   `sql:"name"`
}
