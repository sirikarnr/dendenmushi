package model

type Reminder struct {
	UserID  string
	Title   string
	Amount  float64
	Duedate string
}
