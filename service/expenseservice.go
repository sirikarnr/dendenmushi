package service

import (
	"dendenmushi/model"
	"dendenmushi/repository"
	"fmt"
	"strconv"
	"time"

	"github.com/google/uuid"
)

type ConnectionHandler struct {
	sqlHandler repository.SQLHandler
}

func BotReceiveCmdProocess(expenseinfo *model.Expense) {
	db := repository.DbConnection()
	connection := ConnectionHandler{
		sqlHandler: db,
	}
	connection.AddExpenseInfo(expenseinfo)
	connection.AddUser(expenseinfo.UserID)

	defer connection.sqlHandler.Connection.Close()
}

func (conn ConnectionHandler) AddExpenseInfo(expenseinfo *model.Expense) {
	if expenseinfo != nil {
		id := uuid.New().String()
		expenseinfo.ID = id
		err := conn.sqlHandler.Connection.Insert(expenseinfo)
		if err != nil {
			fmt.Println(err)

		}
	}
}

func (conn ConnectionHandler) AddUser(userID string) {
	user := model.User{
		ID:   userID,
		Name: userID,
	}
	conn.sqlHandler.Connection.Model(&user).OnConflict("DO NOTHING").Insert()
}

func (conn ConnectionHandler) GetUsers() []model.User {
	var users []model.User
	conn.sqlHandler.Connection.Model(&users).
		Select()
	return users
}

//GetExpenseByUserIDAndDuedate : get expense data by userid and tomorrow date
//use tomorrow to find data one day before duedate
func (conn ConnectionHandler) GetExpenseByUserIDAndDuedate(userID, date string) []model.Expense {
	var expenses []model.Expense
	duedate, err := strconv.ParseInt(date, 10, 64)
	if err != nil {
		fmt.Println("invalid date format", err)
	}
	conn.sqlHandler.Connection.Model(&expenses).
		Column("name", "duedate", "amount", "userid").
		Where("userid = ?", userID).
		Where("duedate = ?", fmt.Sprintf("%v", duedate)).
		Select()
	return expenses
}

func BotRemindSchedulerTicker() []model.Reminder {
	db := repository.DbConnection()
	connection := ConnectionHandler{
		sqlHandler: db,
	}
	date := time.Now().AddDate(0, 0, 1)
	users := connection.GetUsers()
	var reminders []model.Reminder
	for _, user := range users {
		expenses := connection.GetExpenseByUserIDAndDuedate(user.ID, date.Format("02"))
		reminders = append(reminders, connection.pushReminderExpense(0, expenses, reminders)...)
	}
	defer connection.sqlHandler.Connection.Close()
	return reminders
}

func (c ConnectionHandler) pushReminderExpense(ind int, expenses []model.Expense, reminders []model.Reminder) []model.Reminder {
	if ind > len(expenses)-1 {
		return reminders
	}
	reminder := model.Reminder{
		UserID:  expenses[ind].UserID,
		Amount:  expenses[ind].Amount,
		Duedate: expenses[ind].DueDate,
		Title:   expenses[ind].Name,
	}
	reminders = append(reminders, reminder)
	ind++
	return c.pushReminderExpense(ind, expenses, reminders)
}
