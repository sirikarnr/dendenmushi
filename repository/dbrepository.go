package repository

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"

	"dendenmushi/constant"
	mod "dendenmushi/model"
)

//SQLHandler : db handler
type SQLHandler struct {
	Connection *pg.DB
}

//DbConnection : database connection
func DbConnection() SQLHandler {
	db := pg.Connect(&pg.Options{
		User:     constant.Postgres,
		Password: constant.Postgres,
		Database: constant.Postgres,
	})

	return SQLHandler{
		Connection: db,
	}
}

//CreateSchema : create table with struct
func (db SQLHandler) CreateSchema() error {
	defer db.Connection.Close()
	for _, model := range []interface{}{
		&mod.Expense{},
		&mod.User{}} {
		err := db.Connection.CreateTable(model, &orm.CreateTableOptions{
			IfNotExists: true,
		})
		if err != nil {
			panic(err)
		}
	}
	return nil
}
