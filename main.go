package main

import (
	"dendenmushi/conf"
	"dendenmushi/handler"
	"dendenmushi/middleware"
	"dendenmushi/repository"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/robfig/cron"
)

func main() {
	conf.InitialConfig()

	finalHandler := http.HandlerFunc(middleware.HandleFunc)
	client := &http.Client{}
	bot, err := linebot.New(os.Getenv("LINE_BOT_CHANNEL_SECRET"), os.Getenv("LINE_BOT_CHANNEL_TOKEN"), linebot.WithHTTPClient(client))
	if err != nil {
		fmt.Println("err: ", err)
		log.Fatal(err)
	}

	middlewareHandler := middleware.MiddlewareHandler{
		Handler:   finalHandler,
		BotClient: bot,
	}
	runScheduler(bot)
	http.Handle("/", middlewareHandler.Middleware())
	http.Handle("/callback", middlewareHandler.MiddlewareBot())

	dbconnect := repository.DbConnection()
	createSchemaErr := dbconnect.CreateSchema()
	if createSchemaErr != nil {
		fmt.Println("create schema failed: ", createSchemaErr)
	}

	http.ListenAndServe(getPort(), nil)
}

func runScheduler(bot *linebot.Client) {
	fmt.Println("runScheduler....")
	handlers := &handler.BotClient{
		Bot: bot,
	}

	c := cron.New(cron.WithLocation(time.UTC))
	c.Start()
	c.AddFunc("@every 30s", func() {
		handlers.RemindExpense()
	})
}

func getPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = "8080"
		fmt.Println("No port in heroku" + port)
	}
	return ":" + port
}
